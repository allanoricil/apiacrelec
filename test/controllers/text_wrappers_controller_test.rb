require 'test_helper'

class TextWrappersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @text_wrapper = text_wrappers(:one)
  end

  test "should get index" do
    get text_wrappers_url
    assert_response :success
  end

  test "should get new" do
    get new_text_wrapper_url
    assert_response :success
  end

  test "should create text_wrapper" do
    assert_difference('TextWrapper.count') do
      post text_wrappers_url, params: { text_wrapper: { line_width: @text_wrapper.line_width, text: @text_wrapper.text } }
    end

    assert_redirected_to text_wrapper_url(TextWrapper.last)
  end

  test "should show text_wrapper" do
    get text_wrapper_url(@text_wrapper)
    assert_response :success
  end

  test "should get edit" do
    get edit_text_wrapper_url(@text_wrapper)
    assert_response :success
  end

  test "should update text_wrapper" do
    patch text_wrapper_url(@text_wrapper), params: { text_wrapper: { line_width: @text_wrapper.line_width, text: @text_wrapper.text } }
    assert_redirected_to text_wrapper_url(@text_wrapper)
  end

  test "should destroy text_wrapper" do
    assert_difference('TextWrapper.count', -1) do
      delete text_wrapper_url(@text_wrapper)
    end

    assert_redirected_to text_wrappers_url
  end
end
