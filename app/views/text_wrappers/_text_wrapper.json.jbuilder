json.extract! text_wrapper, :id, :line_width, :text, :created_at, :updated_at
json.url text_wrapper_url(text_wrapper, format: :json)
