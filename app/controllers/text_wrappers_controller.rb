class TextWrappersController < ApplicationController
  before_action :set_text_wrapper, only: [:show, :edit, :update, :destroy]

  def index
    @text_wrappers = TextWrapper.all
  end

  def show
  end

  def new
    @text_wrapper = TextWrapper.new
  end

  def edit
  end

  def create
    @text_wrapper = TextWrapper.new(text_wrapper_params)
    @text_wrapper.text = wrap(@text_wrapper.text, @text_wrapper.line_width, "<span></span>")
    respond_to do |format|
      if @text_wrapper.save
        format.html { redirect_to @text_wrapper, notice: 'Text wrapper was successfully created.' }
        format.json { render :show, status: :created, location: @text_wrapper }
      else
        format.html { render :new }
        format.json { render json: @text_wrapper.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @text_wrapper.update(text_wrapper_params)
        format.html { redirect_to @text_wrapper, notice: 'Text wrapper was successfully updated.' }
        format.json { render :show, status: :ok, location: @text_wrapper }
      else
        format.html { render :edit }
        format.json { render json: @text_wrapper.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @text_wrapper.destroy
    respond_to do |format|
      format.html { redirect_to text_wrappers_url, notice: 'Text wrapper was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_text_wrapper
      @text_wrapper = TextWrapper.find(params[:id])
    end

    def text_wrapper_params
      params.require(:text_wrapper).permit(:line_width, :text)
    end
    
    def wrap(text, length = 80, character = $/)
        text.scan(/\S.{0,#{length-2}}\S(?=\s|$)|\S+/).join(character)
    end
    
end
