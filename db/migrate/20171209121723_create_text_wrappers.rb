class CreateTextWrappers < ActiveRecord::Migration[5.1]
  def change
    create_table :text_wrappers do |t|
      t.integer :line_width
      t.string :text

      t.timestamps
    end
  end
end
